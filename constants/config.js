/**
 * 配置文件
 */
const Config = {
  appid: 'wxcf006bdd7d69eb49',
  Proxy: 'http://mini.pro/api', //http://localhost:11888
  share: {
    title: '喵喵看书 - 来自一条书虫的分享',
    path: '/pages/book/home/index',
    imageUrl: 'https://book.somethingwhat.com/images/cat-avatar.png'
  },
  version: '2.1.1' //版本号
}

export default Config;